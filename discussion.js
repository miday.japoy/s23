

// CRUD
// Create , Read, Update and Delete

// Create
	// allows us to create documents  under a collection or create
	// a collection if itd oes not exist yet

	// Syntax
	// db.collections.insertOne()
		// allows us to insert or create documeents


db.users.insertOne(
	{
		"firstName": "Tony",
		"lastName": "Stark",
		"username": "iAmIronMan",
		"email": "iloveyou3000@gmail.com",
		"password": "starkIndustries",
		"isAdmin": true
	}
)


	// db.collections.insertMany()
		// allows us to insert or create two ro more documents




db.users.insertMany([
	{
		"firstName": "Pepper",
		"lastName": "Potts",
		"username": "rescueArmor",
		"email": "peper@gmail.com",
		"password": "whereIsTonyAgain",
		"isAdmin": false
	}

	{
		"firstName": "Stever",
		"lastName": "Rogers",
		"username": "theCaptain",
		"email": "captAmerica@gmail.com",
		"password": "iCanLiftMjolnirToo",
		"isAdmin": false
	}

	{
		"firstName": "Thor",
		"lastName": "Odinson",
		"username": "mightyThor",
		"email": "thorNotLoki@gmail.com",
		"password": "imLoki",
		"isAdmin": false
	}
])






// Mini-Activity


db.courses.insertMany([

		{
			"name" : "JavaScript",
			"price" : 3500,
			"description" : "Learn JavaScript in a week!",
			"isActive" : true
		},

		{
			"name" : "HTML",
			"price" : 1000,
			"description" : "LearnBasic HTML in 3 days!",
			"isActive" : true
		},

		{
			"name" : "CSS",
			"price" : 2000,
			"description" : "Make your website fancy, learn CSS now!",
			"isActive" : true
		}




]);


// READ
	// allows us to retrieve data
	// it needs a quesry or filters to specify the document we are retrieving.

	// syntax
		// db.collections.find()
			// allows us ti retrieve all documents in the collection.

	db.users.find();


	// db.collections.findOne({});
		// allow us to find document that matches our criteria


	db.users.find({"isAdmin" : false});



	// db.collections.findOne({});
		// allows us to find first document

	// db.collections.find({});
		//allows us to find all document

	db.users.find({});
	db.users.findOne({});


	// db.collections.find({,})
		// allows us to find the document satisfy all criterias

	db.users.find({"lastName" : "Odinson", "firstName": "Thor"});


// UPDATE

	// allows us to update documents.
	// also use criteria or filter.
	// $set operator

	// db.collerctions.updateOne({}, {$set: {}})
		// allows us ti update one document that satisfy the criteria


	db.users.updateOne({"lastName" : "Potts"}, {$set: {"lastName" : "Stark"}})


	// db.collerctions.updateMany({}, {$set: {}})
		//allows us to update ALL document that satisfy the criteria

	db.users.updateMany({"lastName" : "Odinson"}, {$set: {"isAdmin" : true}})


	// db.collerctions.updateMany({}, {$set: {}})
		// allows us to update the first item in the collection.


	db.users.updateOne({}, {$set: {"email" : "starkIndustries@gmail.com"}});




	// Mini-Activy



	db.courses.updateMany({}, {$set: {"isActive" : false}});

	db.courses.updateMany({}, {$set: {"enrolees" : 10}});


// DELETE
	// allows us ti delete documents.
	//  provide criteria or filters to specify which document to delete from the collection
	//Reminder: be careful when deleting documents , because it will be complicated to retrieve them back again.


	// db.collections.deleteOne({})
		//allows us to delete the first item that matches our criteria


	db.users.deleteOne({"isAdmin" : false});


	// db.collections.deleteMany({})
		//allows us to delete the all item that matches our criteria

	db.users.deleteMany({"isAdmin" : false});


	// db.collections.deleteOne({})
		//allows us to delete the first document.

	db.users.deleteOne({});



	// db.collections.deleteMany({})
		//allows us to delete ALL items in the collections.

	db.users.deleteMany({});



	